import java.util.concurrent.LinkedBlockingQueue;

public class Server implements Runnable {
	private int id;
	private LinkedBlockingQueue<Client> queue = new LinkedBlockingQueue<Client>();;
	private long waiting;
	private GUI gui;

	public Server(GUI gui, int id) {
		this.gui = gui;
		this.id = id;
	}

	public long getWaiting() {
		return waiting;
	}

	public void addClient(Client c) throws InterruptedException {
		queue.put(c);
		waiting += c.getProcess();
		gui.addClient(c,id);
		System.out.println("Client " + c +" is put in the server " + id + " queue");
	}

	public void run() {
		while (true){
			try {
				Client c = queue.take();
				long process = c.getProcess(); 
				Thread.sleep(process);
				waiting = (waiting < process) ? 0 :  waiting-process;
				gui.removeClient(c,id);
				System.out.println("Client " + c +" is removed from the server " + id + " queue");
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
