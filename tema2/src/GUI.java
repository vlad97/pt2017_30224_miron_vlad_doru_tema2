import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private ArrayList<JPanel> panels;
	private HashMap<Client, JLabel> clientLabels = new HashMap<Client, JLabel>();
	
	public GUI(int noServers) {
		
		setTitle("Shop queues");
		setVisible(true);
		setBounds(100, 100, 1000, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		panels = new ArrayList<JPanel>(noServers);
		for (int i=0; i<noServers; i++)
		{	
			JLabel s = new JLabel("Server nr. "+i);
			s.setFont(new Font("Arial", Font.BOLD, 16));
			s.setBounds(50 + 150 *i, 10, 100, 20);
			getContentPane().add(s, BorderLayout.NORTH);
			
			JPanel p = new JPanel();
			p.setBounds(25 + 150*i, 50, 130, 500);			
			panels.add(p);
			getContentPane().add(p);
		}
		
	}

	public void addClient(Client c, int serverId)
	{
		JLabel labelClient = new JLabel(c.toString());
		labelClient.setFont(new Font("Arial", Font.BOLD, 14));		
		labelClient.setForeground(Color.RED);
		clientLabels.put(c, labelClient);
		panels.get(serverId).add(labelClient);	
		panels.get(serverId).revalidate();
	}
	
	public void removeClient(Client c,  int serverId)
	{
		JLabel removeLabel = clientLabels.get(c);
		panels.get(serverId).remove(removeLabel);
		panels.get(serverId).repaint();
		panels.get(serverId).revalidate();
	}
	
	public static void main(String[] args) {
		int noServers = 5;
		GUI gui = new GUI(noServers);
		ClientGenerator start = new ClientGenerator(gui, noServers);
		new Thread(start).start();

	}
}
