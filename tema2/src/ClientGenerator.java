public class ClientGenerator implements Runnable {
	private long previous = 0;
	private long current = 0;
	private Scheduler scheduler;
	private static long MAX_ARRIVAL_TIME=1000;
	private static long MAX_PROCESS_TIME=10000;

	public ClientGenerator(GUI gui, int noServers) {
		scheduler = new Scheduler(gui, noServers);
	}

	public void run() {
		int clientId = 1;
		try {
			while (true) {
				Client c = new Client(clientId, (long)(Math.random()*MAX_ARRIVAL_TIME), (long)(Math.random()*MAX_PROCESS_TIME));
				System.out.println("Client created: " + c);
				scheduler.addClient(c);
				clientId++;
				Thread.sleep(1000);
				previous = c.getArrival();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
