import java.util.ArrayList;

public class Scheduler {
	private ArrayList<Server> allServers = new ArrayList<Server>();
	private int numberOfServers;
	private GUI gui;

	public Scheduler(GUI gui, int noServers) {
		this.gui = gui;
		numberOfServers= noServers;
		for (int i = 0; i < numberOfServers; i++) {
			Server server = new Server(gui, i);
			new Thread(server).start();
			allServers.add(server);
		}
	}

	public void addClient(Client t) throws InterruptedException {
		long minimumWaitingTime=Long.MAX_VALUE;
		Server minimumWTServer = null;
		for (Server server : allServers) {
			if (server.getWaiting() < minimumWaitingTime) {
				minimumWaitingTime = server.getWaiting();
				minimumWTServer = server;
			}
		}	
		minimumWTServer.addClient(t);
	}
}
