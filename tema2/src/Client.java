public class Client {
	private int id;
	private long arrival;
	private long process;

	public Client(int i, long aT, long pT) {
		id = i;
		arrival = aT;
		process = pT;
	}

	public int getId() {
		return id;
	}

	public long getArrival() {
		return arrival;
	}

	public long getProcess() {
		return process;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setArrival(int arrivalTime) {
		this.arrival = arrivalTime;
	}

	public void setProcess(int processTime) {
		this.process = processTime;
	}
	
	public String toString()
	{
		return "(" + id + ": " 	+ arrival + "/" + process + ")";
	}

}
